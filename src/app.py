from fastapi import FastAPI, Response

from src.deployment.deployment_controller import deployment_router

app = FastAPI()

app.include_router(deployment_router)


@app.get("/")
def api() -> Response:
    return Response("Running 🚀", media_type="text/plain")
