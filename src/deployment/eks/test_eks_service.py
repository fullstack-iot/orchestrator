from unittest.mock import AsyncMock

import pytest
from pytest_mock import MockerFixture

from src.deployment.eks.eks_service import (
    create_eks_cluster,
    create_eks_node_group,
    check_eks_create,
    delete_eks_cluster,
    delete_eks_nodegroup,
)
from src.utils import load_config


@pytest.mark.asyncio()
async def test_check_eks_nodegroup(mocker: MockerFixture) -> None:
    # Arrange
    mock_eks = mocker.MagicMock()
    mock_eks.describe_cluster = AsyncMock()
    mock_eks.exceptions.ResourceNotFoundException = Exception
    mock_eks.exceptions.ResourceInUseException = Exception
    mocker.patch("src.deployment.eks.eks_service.check_eks_delete")
    # Act
    await delete_eks_nodegroup("test-cluster", mock_eks)
    # Assert
    mock_eks.delete_nodegroup.assert_called_once_with(
        clusterName="test-cluster", nodegroupName="test-cluster-workers"
    )


@pytest.mark.asyncio()
async def test_delete_eks_cluster(mocker: MockerFixture) -> None:
    # Arrange
    mock_eks = mocker.MagicMock()
    mock_eks.delete_cluster = AsyncMock()
    mock_eks.exceptions.ResourceNotFoundException = Exception
    mock_eks.exceptions.ResourceInUseException = Exception
    mocker.patch("src.deployment.eks.eks_service.check_eks_delete")
    # Act
    await delete_eks_cluster("test-cluster", mock_eks)
    # Assert
    mock_eks.delete_cluster.assert_called_once_with(name="test-cluster")


@pytest.mark.asyncio()
async def test_check_eks_create(mocker: MockerFixture) -> None:
    # Arrange
    mock_eks = mocker.MagicMock()
    mock_eks.describe_cluster = AsyncMock()
    mock_eks.describe_cluster.return_value = {"cluster": {"status": "ACTIVE"}}
    mock_eks.exceptions.ResourceNotFoundException = Exception
    mock_eks.exceptions.ResourceInUseException = Exception
    # Act
    await check_eks_create(mock_eks, "cluster", "test-cluster")
    # Assert
    mock_eks.describe_cluster.assert_called_once_with(name="test-cluster")


@pytest.mark.asyncio()
async def test_create_eks_node_group(mocker: MockerFixture) -> None:
    # Arrange
    cluster_name = "test-cluster"
    cfg = await load_config()
    mock_eks = mocker.MagicMock()
    mock_eks.create_nodegroup = AsyncMock()
    mocker.patch("src.deployment.eks.eks_service.check_eks_create")
    # Act
    await create_eks_node_group(cluster_name, mock_eks, cfg)
    # Assert
    mock_eks.create_nodegroup.assert_called_once_with(
        clusterName=cluster_name,
        nodegroupName=f"{cluster_name}-workers",
        subnets=cfg.subnets,
        instanceTypes=["t3.small"],
        scalingConfig={"minSize": 1, "maxSize": 3, "desiredSize": 2},
        nodeRole=cfg.eks_node_role_arn,
    )


@pytest.mark.asyncio()
async def test_create_eks_cluster(mocker: MockerFixture) -> None:
    # Arrange
    cluster_name = "test-cluster"
    cfg = await load_config()
    mock_eks = mocker.MagicMock()
    mock_eks.create_cluster = AsyncMock()
    mocker.patch("src.deployment.eks.eks_service.check_eks_create")
    # Act
    await create_eks_cluster(cluster_name, mock_eks, cfg)
    # Assert
    mock_eks.create_cluster.assert_called_once_with(
        name=cluster_name,
        roleArn=cfg.eks_role_arn,
        resourcesVpcConfig={
            "subnetIds": cfg.subnets,
            "securityGroupIds": cfg.security_groups,
        },
        version="1.29",
    )
