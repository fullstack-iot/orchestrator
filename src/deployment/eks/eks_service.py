import asyncio
from typing import Literal

import boto3

from src.utils import Config


async def check_eks_delete(
    eks: boto3.client, resource: Literal["cluster", "nodegroup"], cluster_name: str
) -> None:
    while True:
        try:
            if resource == "cluster":
                eks.describe_cluster(name=cluster_name)
            elif resource == "nodegroup":
                eks.describe_nodegroup(
                    clusterName=cluster_name, nodegroupName=f"{cluster_name}-workers"
                )
        except (
            eks.exceptions.ResourceNotFoundException,
            eks.exceptions.ResourceInUseException,
        ):
            break
        await asyncio.sleep(30)


async def delete_eks_nodegroup(cluster_name: str, eks: boto3.client) -> None:
    try:
        eks.delete_nodegroup(
            clusterName=cluster_name,
            nodegroupName=f"{cluster_name}-workers",
        )
    except (
        eks.exceptions.ResourceInUseException,
        eks.exceptions.ResourceNotFoundException,
    ):
        return
    await check_eks_delete(eks, "nodegroup", cluster_name)


async def delete_eks_cluster(cluster_name: str, eks: boto3.client) -> None:
    try:
        eks.delete_cluster(name=cluster_name)
    except (
        eks.exceptions.ResourceInUseException,
        eks.exceptions.ResourceNotFoundException,
    ):
        return
    await check_eks_delete(eks, "cluster", cluster_name)


async def check_eks_create(eks: boto3.client, resource: str, cluster_name: str) -> None:
    while True:
        try:
            if resource == "cluster":
                response = eks.describe_cluster(name=cluster_name)
            elif resource == "nodegroup":
                response = eks.describe_nodegroup(
                    clusterName=cluster_name, nodegroupName=f"{cluster_name}-workers"
                )
            if response[resource]["status"] == "ACTIVE":
                break
        except (
            eks.exceptions.ResourceNotFoundException,
            eks.exceptions.ResourceInUseException,
        ):
            break
        await asyncio.sleep(30)


async def create_eks_node_group(
    cluster_name: str,
    eks: boto3.client,
    cfg: Config,
) -> None:
    try:
        eks.create_nodegroup(
            clusterName=cluster_name,
            nodegroupName=f"{cluster_name}-workers",
            subnets=cfg.subnets,
            scalingConfig={
                "minSize": 1,
                "maxSize": 3,
                "desiredSize": 2,
            },
            instanceTypes=["t3.small"],
            nodeRole=cfg.eks_node_role_arn,
        )
    except (
        eks.exceptions.ResourceInUseException,
        eks.exceptions.ResourceNotFoundException,
    ):
        return
    await check_eks_create(eks, "nodegroup", cluster_name)


async def create_eks_cluster(cluster_name: str, eks: boto3.client, cfg: Config) -> None:
    try:
        eks.create_cluster(
            name=cluster_name,
            roleArn=cfg.eks_role_arn,
            resourcesVpcConfig={
                "subnetIds": cfg.subnets,
                "securityGroupIds": cfg.security_groups,
            },
            version="1.29",
        )
    except (
        eks.exceptions.ResourceInUseException,
        eks.exceptions.ResourceNotFoundException,
    ):
        return
    await check_eks_create(eks, "cluster", cluster_name)
