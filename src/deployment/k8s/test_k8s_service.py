from pytest_mock import MockerFixture

from src.deployment.k8s.k8s_service import (
    link_kubeconfig,
    get_container_configs,
)
from src.shared.models import PortMap


def test_get_container_configs() -> None:
    config = get_container_configs()[0]
    assert config.name == "domain-api"
    assert config.image == "fsiot/domain-api"
    assert config.ports == [PortMap(internal=3000, external=3000)]


def test_link_kubeconfig(mocker: MockerFixture) -> None:
    mock_subprocess = mocker.patch("subprocess.run")
    mock_k8s_config = mocker.patch("kubernetes.config")
    link_kubeconfig("cluster_name", mock_k8s_config)
    mock_subprocess.assert_called_once_with(
        [
            "aws",
            "eks",
            "update-kubeconfig",
            "--name",
            "cluster_name",
            "--kubeconfig",
            "/tmp/cluster_name.yml",
        ]
    )
    mock_k8s_config.load_kube_config.assert_called_once_with(
        config_file="/tmp/cluster_name.yml"
    )
