import subprocess
from pathlib import Path
from typing import List

import kubernetes as k8s
import yaml

from src.shared.models import ContainerConfig, DockerCompose, PortMap


def get_container_configs() -> List[ContainerConfig]:
    file_string = Path.open(Path("docker-compose.yml")).read()
    file_dict = yaml.safe_load(file_string)
    docker_compose = DockerCompose(**file_dict)
    return [
        ContainerConfig(
            name=svc.container_name,
            image=svc.image,
            ports=[
                PortMap(
                    internal=int(port.split(":")[1]), external=int(port.split(":")[0])
                )
                for port in svc.ports or []
            ],
            env=(
                [{"name": k, "value": v} for k, v in svc.environment.items()]
                if svc.environment
                else []
            ),
        )
        for svc in docker_compose.services.values()
    ]


def create_k8s_services(client: k8s.client) -> None:
    container_configs = get_container_configs()
    for c in container_configs:
        service = client.V1Service(
            api_version="v1",
            kind="Service",
            metadata=client.V1ObjectMeta(name=f"{c.name}-service"),
            spec=client.V1ServiceSpec(
                selector={"app": c.name},
                ports=[
                    client.V1ServicePort(
                        protocol="TCP", port=p.internal, target_port=p.external
                    )
                    for p in c.ports
                ],
                type="LoadBalancer",
            ),
        )
        api_instance = client.CoreV1Api()
        api_instance.create_namespaced_service(
            body=service,
            namespace="default",
        )


def create_k8s_deployments(client: k8s.client) -> None:
    container_configs = get_container_configs()
    for c in container_configs:
        container = client.V1Container(
            name=c.name,
            image=c.image,
            ports=[client.V1ContainerPort(container_port=p.internal) for p in c.ports],
            env=[client.V1EnvVar(name=e["name"], value=e["value"]) for e in c.env],
        )
        template = client.V1PodTemplateSpec(
            metadata=client.V1ObjectMeta(labels={"app": c.name}),
            spec=client.V1PodSpec(containers=[container]),
        )
        spec = client.V1DeploymentSpec(
            replicas=1,
            template=template,
            selector={"matchLabels": {"app": c.name}},
        )
        deployment = client.V1Deployment(
            api_version="apps/v1",
            kind="Deployment",
            metadata=client.V1ObjectMeta(name=f"{c.name}-deployment"),
            spec=spec,
        )
        api_instance = client.AppsV1Api()
        api_instance.create_namespaced_deployment(
            body=deployment,
            namespace="default",
        )


def link_kubeconfig(cluster_name: str, config: k8s.config) -> None:
    subprocess.run(
        [
            "aws",
            "eks",
            "update-kubeconfig",
            "--name",
            cluster_name,
            "--kubeconfig",
            f"/tmp/{cluster_name}.yml",
        ],
    )
    config.load_kube_config(config_file=f"/tmp/{cluster_name}.yml")
