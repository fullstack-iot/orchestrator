import boto3
from fastapi import APIRouter, BackgroundTasks, Depends, Response

from src.deployment.deployment_service import (
    create_cloud_deployment,
    delete_cloud_deployment,
)
from src.shared.models import Environment
from src.utils import Config, load_config

deployment_router = APIRouter()


async def get_eks_client() -> boto3.client:
    return boto3.client("eks")


@deployment_router.post("/sites/{site_name}/deployments/{environment}")
async def create_deployment(
    background_tasks: BackgroundTasks,
    site_name: str,
    environment: Environment,
    cfg: Config = Depends(load_config),
    eks: boto3.client = Depends(get_eks_client),
) -> Response:
    cluster_name = f"{site_name}-{environment}"
    if environment in ("staging", "production-cloud"):
        background_tasks.add_task(create_cloud_deployment, cluster_name, cfg, eks)
    return Response(
        f"{cluster_name} creating. this takes about 10 minutes.",
        status_code=202,
        media_type="text/plain",
    )


@deployment_router.delete("/sites/{site_name}/deployments/{environment}")
async def delete_deployment(
    background_tasks: BackgroundTasks,
    site_name: str,
    environment: Environment,
    eks: boto3.client = Depends(get_eks_client),
) -> Response:
    if environment in ("staging", "production-cloud"):
        background_tasks.add_task(delete_cloud_deployment, site_name, environment, eks)
    return Response(
        status_code=204,
    )
