import boto3
from kubernetes import config, client

from src.deployment.eks.eks_service import (
    create_eks_cluster,
    create_eks_node_group,
    delete_eks_cluster,
    delete_eks_nodegroup,
)
from src.deployment.k8s.k8s_service import (
    link_kubeconfig,
    create_k8s_deployments,
    create_k8s_services,
)
from src.shared.models import Environment
from src.utils import Config


async def delete_cloud_deployment(
    site_name: str,
    environment: Environment,
    eks: boto3.client,
) -> None:
    cluster_name = f"{site_name}-{environment}"
    await delete_eks_nodegroup(cluster_name, eks)
    await delete_eks_cluster(cluster_name, eks)


async def create_cloud_deployment(
    cluster_name: str,
    cfg: Config,
    eks: boto3.client,
) -> None:
    await create_eks_cluster(cluster_name, eks, cfg)  # this takes 8-10 minutes
    await create_eks_node_group(cluster_name, eks, cfg)  # this takes 1-2 minutes
    link_kubeconfig(cluster_name, config)
    create_k8s_deployments(client)
    create_k8s_services(client)
