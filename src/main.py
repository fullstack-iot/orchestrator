import asyncio
import logging

import uvicorn

from src.utils import load_config, setup_logger


async def main() -> None:
    config = await load_config()
    setup_logger(config.log_level)
    logging.info(f"Running  with Config:  {config}")
    if __name__ == "__main__":
        uvicorn.run(
            "src.app:app",
            host=str(config.host),
            port=config.port,
            reload=config.uvicorn_reload,
            log_level=config.log_level.lower(),
        )


asyncio.run(main())
