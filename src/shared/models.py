from typing import Literal, Dict, List, Optional

from pydantic import BaseModel

Environment = Literal["staging", "production-cloud", "production-onprem"]


class Service(BaseModel):
    container_name: str
    image: str
    ports: Optional[List[str]] = []
    environment: Optional[Dict[str, str]] = {}


class DockerCompose(BaseModel):
    version: str
    services: Dict[str, Service]
    volumes: Dict[str, None]


class PortMap(BaseModel):
    internal: int  # also known as container
    external: int  # also known as target


class ContainerConfig(BaseModel):
    name: str
    image: str
    env: List[Dict[str, str]]
    ports: List[PortMap]
