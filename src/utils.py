import logging
import os
from ipaddress import IPv4Address
from typing import List, Literal

import aiofiles
import yaml
from pydantic import BaseModel
from pydantic_settings import BaseSettings


class Config(BaseSettings):
    version: str
    host: IPv4Address
    uvicorn_reload: bool = False
    port: int
    e2e: bool
    log_level: Literal["ERROR", "WARN", "INFO", "DEBUG"] = "DEBUG"
    eks_role_arn: str
    eks_node_role_arn: str
    subnets: List[str]
    security_groups: List[str]


class ConfigMap(BaseModel):
    development: Config
    staging: Config
    production: Config


async def load_config() -> Config:
    async with aiofiles.open("cfg.yml", mode="rb") as file:
        contents = await file.read()
        config_map = ConfigMap(**yaml.safe_load(contents))
        environment = os.environ.get("ENV", "local")
        match environment:
            case "development":
                return config_map.development
            case "staging":
                return config_map.staging
            case _:
                return config_map.production


def setup_logger(level: str) -> None:
    # adds color
    for ind, lvl in enumerate(
        [logging.ERROR, logging.INFO, logging.WARNING, logging.DEBUG],
    ):
        logging.addLevelName(
            lvl,
            f"\033[0;3{ind + 1}m%s\033[1;0m" % logging.getLevelName(lvl),
        )
    # inits logger
    logging.basicConfig(
        encoding="utf-8",
        format="%(levelname)s: %(message)s",
        level=level,
    )
