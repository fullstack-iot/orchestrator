FROM python:3.12.1-slim-bookworm
WORKDIR /app
COPY cfg.yml .
COPY src src
COPY Pipfile* ./
RUN pip install pipenv
RUN rm -f src/test_*
RUN pipenv install --system --deploy
CMD ["python", "-m", "src.main"]
