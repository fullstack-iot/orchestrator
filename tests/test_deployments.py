import os

import boto3
from fastapi.testclient import TestClient
from moto import mock_aws
from pytest_mock import MockerFixture

from src.app import app

SITE_NAME = "test-site"


client = TestClient(app)

"""Mocked AWS Credentials for moto."""
os.environ["AWS_ACCESS_KEY_ID"] = "testing"
os.environ["AWS_SECRET_ACCESS_KEY"] = "testing"  # noqa: S105
os.environ["AWS_SECURITY_TOKEN"] = "testing"  # noqa: S105
os.environ["AWS_SESSION_TOKEN"] = "testing"  # noqa: S105
os.environ["AWS_DEFAULT_REGION"] = "us-east-1"


def mock_create_eks() -> None:
    eks = boto3.client("eks")
    eks.create_cluster(
        name=f"{SITE_NAME}-staging",
        version="1.29",
        roleArn="arn:aws:iam::123456789012:role/eks-role",
        resourcesVpcConfig={"subnetIds": ["subnet-0a1b2c3d4e5f67890"]},
    )
    eks.create_nodegroup(
        clusterName=f"{SITE_NAME}-staging",
        nodegroupName=f"{SITE_NAME}-staging-workers",
        subnets=["subnet-0a1b2c3d4e5f67890"],
        instanceTypes=["t3.small"],
        scalingConfig={"minSize": 1, "maxSize": 3, "desiredSize": 2},
        nodeRole="arn:aws:iam::123456789012:role/eks-node-role",
    )


def mock_k8s(mocker: MockerFixture) -> None:
    mocker.patch("src.deployment.deployment_service.link_kubeconfig")
    mocker.patch("src.deployment.deployment_service.create_k8s_deployments")
    mocker.patch("src.deployment.deployment_service.create_k8s_services")


@mock_aws()
def test_create_staging_deployment(mocker: MockerFixture) -> None:
    mock_create_eks()
    mock_k8s(mocker)
    response = client.post(
        f"/sites/{SITE_NAME}/deployments/staging",
    )
    assert response.status_code == 202


def mock_delete_eks() -> None:
    eks = boto3.client("eks")
    eks.delete_nodegroup(
        clusterName=f"{SITE_NAME}-staging",
        nodegroupName=f"{SITE_NAME}-staging-workers",
    )
    eks.delete_cluster(name=f"{SITE_NAME}-staging")


@mock_aws()
def test_delete_deployment() -> None:
    mock_create_eks()
    mock_delete_eks()
    response = client.delete(
        f"/sites/{SITE_NAME}/deployments/staging",
    )
    assert response.status_code == 204
