from setuptools import find_packages, setup

setup(
    name="deployment-tooling",
    version="1.0.0-beta",
    packages=find_packages(),
    licence="UNLICENSED",
)
