# REST API 
![](https://img.shields.io/badge/python%203.12-gray?style=for-the-badge&logo=python)
![](https://img.shields.io/badge/web_framework-fastapi-27a699?style=for-the-badge)
![](https://img.shields.io/badge/ORM-sqlmodel-8e56c2?style=for-the-badge)
![](https://img.shields.io/gitlab/coverage/fullstack-iot/deployment-tooling/00-scaffolding?logo=pytest&style=for-the-badge)






##  Sequence Diagram

```plantuml
participant Runner
participant Registry
participant Deployer
participant User
participant "Environment Servers\n(onprem/cloud)" as Servers
Runner -> Registry: Pushes new images
Registry -> Deployer: Webhook notification\n(new image available)
alt Automated Deployment
    Deployer -> Servers: Deploy updated containers
else Scheduled Deployment
    Deployer -> Servers: Deploy at scheduled time
else Manual Deployment
    User -> Deployer: Trigger manual deployment
    Deployer -> Servers: Deploy updated containers
end
```

## REST API Design

The REST API facilitates deployment operations, with key endpoints for deploying and checking the status of deployments across site environments.

### Notable Endpoints

1. Deployments
	- Trigger Deployment
		- `POST /sites/{siteName}/deployments`
		- Description: Triggers a manual deployment 
   		- Payload:
		```json
		{
		  "deployment_type": "scheduled | manual | automatic",
		  "environment": "staging | production-cloud | production-onprem"
		}
		```
	- Check Deployment Status
		- `GET /sites/{siteName}/deployments/status`
		- Description: Retrieves the status of the most recent deployment for a specified site environment.
		- Query Parameters:
			- `environment: staging | production-cloud | production-onprem`
	- Delete Deployment
		- `DELETE /sites/{siteName}/deployments/{environment}`
		- Description: Deletes deployments




## Folder Structure

```
deployment-tooling/
│
├── src/
│   ├── __init__.py
│   ├── main.py                 # Entry point for the FastAPI application
│   ├── utils.py.py             # Entry point for the FastAPI application
│   │
│   ├── site/                   # Site module
│   │   ├── site_models.py      # Site entities
│   │   ├── site_schemas.py     # Site DTOs
│   │   ├── site_controller.py  # Site-related API routes and view logic
│   │   └── site_service.py     # Business logic for customer management
│   │
│   ├── deployment/             # Deployment module
│   │   ├── deployment_models.py      # Deployment entities
│   │   ├── deployment_schemas.py     # Deployment DTOs
│   │   ├── deployment_controller.py  # Deployment-related API routes and view logic
│   │   └── deployment_service.py      # Business logic for deployment operations
│   │
│   └── shared/                 # Shared module
│       └── shared_models.py    # Shared entities
│
├── tests/                      # Integration Test suites
│   ├── __init__.py
│   ├── test_site.py
│   └── test_deployment.py
│
├── Pipfile                     # Pipenv file for managing dependencies
├── Pipfile.lock
└── README.md
```

