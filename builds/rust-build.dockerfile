FROM rust:1.79.0-slim-bookworm
RUN apt update; apt install -y pkg-config libssl-dev cmake build-essential
ENV OPENSSL_LIB_DIR=/usr/lib/x86_64-linux-gnu
ENV OPENSSL_INCLUDE_DIR=/usr/include
RUN rustup toolchain install nightly; \
    rustup component add rustfmt; \
    rustup component add clippy; \
    cargo install cargo-cmd; \
    cargo install cargo-audit; \
    cargo install cargo-tarpaulin; \
    cargo install cargo-udeps --locked; \
    cargo install cargo2junit;
